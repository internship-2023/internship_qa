FROM mcr.microsoft.com/playwright/python:v1.30.0-focal

COPY . /test_project

WORKDIR /test_project
RUN python -m pip install --upgrade pip

RUN pip install -r requirements.txt


