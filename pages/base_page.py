from __future__ import annotations
import re
import allure
from playwright.sync_api import Page, Response, expect



class BasePage:
    def __init__(self, page: Page) -> None:
        self.page = page

    def visit(self, url: str) -> Response | None:
        with allure.step(f'Opening the url "{url}"'):
            return self.page.goto(url, wait_until='networkidle')

    def reload(self) -> Response | None:
        with allure.step(f'Reloading page with url "{self.page.url}"'):
            return self.page.reload(wait_until='domcontentloaded')

    def should_be_open(self, url):
        with allure.step(f'Checking that  "{url}"  page is open'):
            expect(self.page).to_have_url(re.compile(url))

