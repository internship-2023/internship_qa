from playwright.sync_api import Page
from pages.base_page import BasePage
from page_factory.link import Link


class Base86StartPage(BasePage):
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.sign_in_link = Link(page, locator="link", name="Sign In")

    def sign_in(self) -> None:
        self.sign_in_link.get_link_locator().click()
