from playwright.sync_api import Page
from pages.base_page import BasePage
from page_factory.input import Input
from page_factory.button import Button
from page_factory.menuitem import MenuItem
from page_factory.heading import Heading
from page_factory.row import Row
from page_factory.presentation import Presentation
from page_factory.label import Label
from page_factory.table import Table
from settings import MAX_QTY


class Base86PageForCustomerStaging(BasePage):
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.some_order_number = None
        self.list_of_status = None
        self.supplier_row = None
        self.order_number = None
        self.supplier = None
        self.order_row = None
        self.orders_row_products = None
        self.column_for_qty = None
        self.sign_in_input_login = Input(page, locator="", name="Email")
        self.sign_in_input_password = Input(page, locator="", name="Password")
        self.sign_in_button = Button(page, locator="button", name="Login")
        self.menu_heading_PO = Heading(page, locator="heading", name="Orders")
        self.menu_PO = MenuItem(page, locator="menuitem", name="Purchase Orders")
        self.first_row = Row(page, locator="row", name="//tbody//tr[1]")
        self.view_details_button = Button(page, locator="button", name="View Details")
        self.view_add_button = Button(page, locator="button", name="Add")
        self.menu_supplier = MenuItem(page, locator="menuitem", name="Suppliers")
        self.order_list_button = Button(page, locator="button", name="View order list")
        self.heading_order_list = Heading(page, locator="heading", name="Orders")
        self.presentation_of_order_list_modal = Presentation(page, locator="div", name="presentation")
        self.heading_new_order = Heading(page, locator="heading", name="Edit Purchase Order")
        self.filter_button = Button(page, locator="button", name="Filter")
        self.label_of_status = Label(page, locator="label", name="")
        self.some_button = Button(page, locator="button", name="")
        self.search_supplier_button = Button(page, locator="div:nth-child(3) >*> button:nth-child(4)", name="")
        self.search_for_supplier = Input(page, locator="", name="Type here...")
        self.edit_order_button = Button(page, locator="button", name="Edit Order")
        self.select_proceed_button = Button(page, locator="button", name="Select & Proceed")
        self.table_body = Table(page, locator="//table//tbody", name="")
        self.table_head = Table(page, locator="//table//thead", name="")
        self.edit_qty_button = Button(page, locator="button", name="Edit")
        self.input_for_qty = Input(page, locator="input", name="")
        self.save_qty_button = Button(page, locator="button", name="Save")
        self.remove_product_from_order_button = Button(page, locator="button", name="Remove from PO")

    def log_in(self, login, password) -> None:
        self.sign_in_input_login.fill(login)
        self.sign_in_input_password.fill(password)
        self.sign_in_button.get_button_locator().click()

    def move_to_po(self):
        self.menu_heading_PO.get_heading_locator().click()
        try:
            self.menu_PO.get_menu_item_locator().click()
        except:
            pass

    def choose_order(self):
        self.first_row.get_by_xpath(f'{self.table_body.locator}//tr').get_by_role("button")
        self.order_number = self.first_row.get_by_xpath(f'{self.table_body.locator}//tr//td[1]/div/div')
        self.order_number.click()

    def get_some_order_number(self):
        self.page.wait_for_timeout(3000)
        last_order = self.page.locator("tr").count() - 1
        self.some_order_number = self.page.locator("tr").nth(last_order).locator("td").nth(1).text_content()

    def view_details(self):
        frame_locator = self.view_details_button.get_button_locator()
        self.should_be_open(frame_locator.page.url)

    def move_to_supplier(self):
        self.menu_supplier.get_menu_item_locator().click()

    def choose_supplier(self):
        new_row = self.page.locator("tr").nth(2)
        self.supplier_row = new_row
        new_row.get_by_role("button").click()

    def view_order_list(self):
        frame_locator = self.order_list_button.get_button_locator()
        frame_locator.click()
        modal_view = self.presentation_of_order_list_modal.get_locator_h3(self.supplier).nth(0)
        self.presentation_of_order_list_modal.object_should_contain_text(modal_view.text_content(), modal_view)

    def find_by_number(self):
        self.page.get_by_placeholder("Search").fill(self.some_order_number)
        new_row = self.page.locator("tr").nth(2)
        self.first_row.object_should_contain_text(self.some_order_number, new_row)

    def add_new_order(self):
        self.view_add_button.get_button_locator().click()
        new_order_locator = self.heading_new_order.get_heading_locator()
        self.heading_new_order.object_should_be_visible(new_order_locator)

    def open_filter_menu(self):
        self.filter_button.get_button_locator().click()
        self.get_list_of_statuses()

    def choose_status(self, specified_status=None):
        if specified_status is not None:
            self.page.get_by_label(specified_status).check()
            self.some_button.get_button_by_name("Apply").click()
        else:
            for status_id in range(self.list_of_status.count()):
                status = self.list_of_status.nth(status_id).text_content()
                self.page.get_by_label(status).check()
                self.some_button.get_button_by_name("Apply").click()
                break

    def get_list_of_statuses(self):
        self.list_of_status = self.label_of_status.get_label_locator("")

    def check_filtered_page(self):
        pass

    def select_supplier_for_filter(self, supplier):
        self.search_supplier_button.get_locator_Object().click()
        self.search_for_supplier.get_input_locator().fill(supplier)
        self.label_of_status.get_label_locator(supplier).check()
        self.some_button.get_button_by_name("Apply").click()

    def choose_order_for_edit(self):

        header_row = self.first_row.get_by_xpath_(f'{self.table_head.locator}//tr[1]//th')
        orders_row_products = self.first_row.get_by_xpath_(f'{self.table_body.locator}//tr')

        # need to get number of products column
        column_for_products = self.get_position_in_table(header_row, 'Products')

        # need to get number of order number column
        column_for_number = self.get_position_in_table(header_row, 'Number')

        #  using number of products we'll find order that has, at least, some products
        for order in range(1, orders_row_products.count() + 1):
            orders_row = self.first_row.get_by_xpath_(f'{self.table_body.locator}//tr[{order}]//td')
            try:
                if int(orders_row.nth(column_for_products).text_content()) > 0:
                    new_row = self.page.locator(f'{self.table_body.locator}//tr[{order}]')
                    self.some_order_number = orders_row.nth(column_for_number).text_content()
                    new_row.get_by_role("button").click()
                    break
            except:
                pass

        edit_order_locator = self.edit_order_button.get_button_locator()
        edit_order_locator.click()

    def change_qty_of_products(self):

        self.click_to_select_and_proceed_button()

        edit_qty_button_locator = self.edit_qty_button.get_button_locator()
        edit_qty_button_locator.click()

        self.orders_row_products = self.first_row.get_by_xpath_(f'{self.table_body.locator}//tr')
        header_row = self.first_row.get_by_xpath_(f'{self.table_head.locator}//tr[1]//th')

        # need to get number of products column
        self.column_for_qty = self.get_position_in_table(header_row, 'Qty')

        #  using number of products we'll find order that has, at least, some products
        for order in range(1, self.orders_row_products.count() + 1):
            orders_row = self.first_row.get_by_xpath_(f'{self.table_body.locator}//tr[{order}]//td')
            there_is_input = False
            qty_input_in_current_row = orders_row.nth(self.column_for_qty).locator('input')

            try:
                qty_input_in_current_row.fill(MAX_QTY)
                there_is_input = True
            except TimeoutError:
                pass
            finally:
                if there_is_input:
                    self.input_for_qty.object_should_have_value(MAX_QTY, qty_input_in_current_row)

        self.save_qty_button.get_button_locator().click()

    def get_position_in_table(self, row, column_name):
        for iter_for_columns in range(row.count()):
            if row.locator(':scope').nth(iter_for_columns).text_content() == column_name:
                return iter_for_columns

    def save_changed_order(self):
        self.save_qty_button.get_button_locator().click()

        #  using number of products we'll find order that has, at least, some products
        self.page.get_by_placeholder("Search").fill(self.some_order_number)
        new_row = self.page.locator(f'{self.table_body.locator}//tr[1]')
        new_row.get_by_role("button").click()

        edit_order_locator = self.edit_order_button.get_button_locator()
        edit_order_locator.click()

        select_proceed_locator = self.select_proceed_button.get_button_locator()
        select_proceed_locator.click()

        for order in range(1, self.orders_row_products.count() + 1):
            orders_row = self.first_row.get_by_xpath_(f'{self.table_body.locator}//tr[{order}]//td')
            there_is_input = False
            qty_input_in_current_row = orders_row.nth(self.column_for_qty).locator('input')

            try:
                some_value = qty_input_in_current_row.input_value()
                there_is_input = True

            except TimeoutError:
                pass
            finally:
                if there_is_input:
                    self.input_for_qty.object_should_have_value(MAX_QTY, qty_input_in_current_row)

    def remove_product_from_order(self):
        self.click_to_select_and_proceed_button()
        self.orders_row_products = self.first_row.get_by_xpath_(f'{self.table_body.locator}//tr')
        # if our table is empty it has 1 row [tr] and 1 column [td] - for some button
        # if our table is not empty, but it has 1 row [tr] with product - it has  more than 1 column [td]

        count_of_row_before_deleting = self.orders_row_products.count()
        products_row = self.first_row.get_by_xpath_(f'{self.table_body.locator}//tr[1]//td')

        products_row.get_by_role("button").click()
        self.remove_product_from_order_button.get_button_locator().click()
        count_of_columns = products_row.count()

        if count_of_columns == 1:
            count_of_row_after_deleting = 0
        else:
            count_of_row_after_deleting = self.orders_row_products.count()

        self.table_body.object_should_have_count_a_less_count_b(count_of_row_after_deleting,
                                                                count_of_row_before_deleting)

    def click_to_select_and_proceed_button(self):
        """
        Find a click to "Select & Proceed"  button on after clicking on "Edit PO" button

        """
        select_proceed_locator = self.select_proceed_button.get_button_locator()
        select_proceed_locator.click()
