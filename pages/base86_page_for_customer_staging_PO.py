from playwright.sync_api import Page

from pages.base_page import BasePage
from page_factory.input import Input
from page_factory.button import Button

class Base86PageForCustomerStagingPO(BasePage):
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.sign_in_input_login = Input(page, locator="", name="Email")
        self.sign_in_input_password = Input(page, locator="", name="Password")
        self.sign_in_button = Button(page, locator="button", name="Login")

    # def get_input_locator(self):
    #     current_page = self.page
    #     return current_page.get_by_placeholder("Email")

    def log_in(self, login, password) -> None:
        self.sign_in_input_login.fill(login)
        self.sign_in_input_password.fill(password)
        self.sign_in_button.get_button_locator().click()
