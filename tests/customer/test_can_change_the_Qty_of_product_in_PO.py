import pytest
from pages.base86_start_page import Base86StartPage
from pages.base86_page_for_customer_staging import Base86PageForCustomerStaging
from settings import BASE_URL_STAGE, URL_SIGN_IN, DRAFT_STATUS, \
    URL_STAGING_CustomerProducts, URL_STAGING_Customer_PO, SUPPLIER


class TestChangeQTyOfProductInPO:
    def test_change_the_QTy_in_order(self, credentials_for_customer_staging,
                                  base86_start_page: Base86StartPage,
                                  base86_page_for_customer_staging: Base86PageForCustomerStaging):
        """
        Customer can change the Qty of product in PO
        C_0025
        """
        login, password = credentials_for_customer_staging
        base86_start_page.visit(BASE_URL_STAGE)
        base86_start_page.sign_in()
        base86_start_page.should_be_open(URL_SIGN_IN)
        base86_page_for_customer_staging.log_in(login, password)
        base86_page_for_customer_staging.should_be_open(URL_STAGING_CustomerProducts)
        base86_page_for_customer_staging.move_to_po()
        base86_page_for_customer_staging.should_be_open(URL_STAGING_Customer_PO)
        base86_page_for_customer_staging.open_filter_menu()
        base86_page_for_customer_staging.choose_status(DRAFT_STATUS)
        base86_page_for_customer_staging.open_filter_menu()
        base86_page_for_customer_staging.select_supplier_for_filter(SUPPLIER)

        base86_page_for_customer_staging.choose_order_for_edit()
        base86_page_for_customer_staging.change_qty_of_products()
        base86_page_for_customer_staging.save_changed_order()
