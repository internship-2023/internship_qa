import pytest

from pages.base86_page_for_customer_staging import Base86PageForCustomerStaging
from pages.base86_start_page import Base86StartPage

from settings import BASE_URL_STAGE, URL_SIGN_IN, \
    URL_STAGING_CustomerProducts, URL_STAGING_Customer_PO


class TestCustomerCanCreatePOByUsingAddButton:

    def test_find_order_by_number(self, credentials_for_customer_staging,
                                  base86_start_page: Base86StartPage,
                                  base86_page_for_customer_staging: Base86PageForCustomerStaging):
        """
        Customer can create new PO using Add button
        C_0024
        """

        login, password = credentials_for_customer_staging

        base86_start_page.visit(BASE_URL_STAGE)
        base86_start_page.sign_in()
        base86_start_page.should_be_open(URL_SIGN_IN)
        base86_page_for_customer_staging.log_in(login, password)
        base86_page_for_customer_staging.should_be_open(URL_STAGING_CustomerProducts)

        base86_page_for_customer_staging.move_to_po()
        base86_page_for_customer_staging.should_be_open(URL_STAGING_Customer_PO)
        base86_page_for_customer_staging.add_new_order()
