allure-pytest==2.8.18
pytest==7.2.1
pytest-playwright==0.3.0
pytest-xdist==3.1.0
python-dotenv==1.0.0

