from page_factory.component import Component


class Presentation(Component):
    @property
    def type_of(self) -> str:
        return 'presentation'


    def get_heading_by_role(self, name_):
        return self.page.get_by_role("heading", name=name_)


    def get_locator_h3(self, name_):
        return self.page.locator("h3")

     # r='h2#{language}', name='Language title'
