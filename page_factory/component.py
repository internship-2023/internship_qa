from abc import ABC, abstractmethod

import allure
from playwright.sync_api import Locator, Page, expect


class Component(ABC):
    def __init__(self, page: Page, locator: str, name: str) -> None:
        self.page = page
        self.name = name
        self.locator = locator

    @property
    @abstractmethod
    def type_of(self) -> str:
        return 'component'

    def get_locator(self, **kwargs) -> Locator:
        locator = self.locator.format(**kwargs)
        return self.page.locator(locator)

    def click(self, **kwargs) -> None:
        with allure.step(f'Clicking {self.type_of} with name "{self.name}"'):
            locator = self.get_locator(**kwargs)
            locator.click()

    def should_be_visible(self, **kwargs) -> None:
        with allure.step(f'Checking that {self.type_of} "{self.name}" is visible'):
            locator = self.get_locator(**kwargs)
            expect(locator).to_be_visible()

    def should_have_text(self, text: str, **kwargs) -> None:
        with allure.step(f'Checking that {self.type_of} "{self.name}" has text "{text}"'):
            locator = self.get_locator(**kwargs)
            expect(locator).to_have_text(text)

    # def object_should_be_visible(self, obj) -> None:
    #     with allure.step(f'Checking that {self.type_of} "{self.name}" is visible'):
    #         expect(obj).to_be_visible()

    def object_should_have_text(self, text: str, obj) -> None:
        with allure.step(f'Checking that {self.type_of} "{self.name}" has text "{text}"'):
            expect(obj).to_have_text(text)

    def get_by_xpath(self, xpath) -> Locator:
        # with allure.step(f'Checking that {self.type_of} "{self.name}" is visible'):
        return self.page.locator(xpath).nth(1)
        # expect(locator).to_be_visible()

    def object_should_be_visible(self, obj) -> None:
        with allure.step(f'Checking that {self.type_of} "{self.name}" is visible'):
            # locator = self.get_locator(**kwargs)
            expect(obj).to_be_visible()

    def object_should_contain_text(self, text: str, obj) -> None:
        with allure.step(f'Checking that {self.type_of} "{self.name}" has text "{text}"'):
            expect(obj).to_contain_text(text)

    def get_locator_Object(self) -> Locator:
        # locator = self.locator.format(**kwargs)
        return self.page.locator(self.locator)

    def get_by_xpath_(self, xpath) -> Locator:
        # with allure.step(f'Checking that {self.type_of} "{self.name}" is visible'):
        return self.page.locator(xpath)

    def object_should_have_value(self, text: str, obj) -> None:
        with allure.step(f'Checking that {self.type_of} "{self.name}" has text "{text}"'):
            expect(obj).to_have_value(text)

    def object_should_have_count_a_less_count_b(self, count_a: int, count_b: int) -> None:
        with allure.step(f'Checking that {self.type_of} "{self.name}" has count less than "{count_a}"'):
            assert count_a < count_b