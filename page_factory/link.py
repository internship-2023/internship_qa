from playwright.sync_api import Locator

from page_factory.component import Component


class Link(Component):
    @property
    def type_of(self) -> str:
        return 'link'

    def get_link_locator(self) -> Locator:
        return self.page.get_by_role("link", name=self.name)
