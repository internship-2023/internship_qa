from page_factory.component import Component


class Heading(Component):
    @property
    def type_of(self) -> str:
        return 'heading'

    def get_heading_locator(self):
        return self.page.get_by_role("heading", name=self.name)
