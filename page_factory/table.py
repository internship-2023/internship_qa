from page_factory.component import Component


class Table(Component):
    @property
    def type_of(self) -> str:
        return 'table'

