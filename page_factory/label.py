from playwright.sync_api import Locator

from page_factory.component import Component


class Label(Component):
    @property
    def type_of(self) -> str:
        return 'label'

    def get_label_locator(self, status) -> Locator:
        return self.page.locator("label", has_text=status)

