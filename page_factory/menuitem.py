from page_factory.component import Component


class MenuItem(Component):
    @property
    def type_of(self) -> str:
        return 'menuitem'

    def get_menu_item_locator(self):
        return self.page.get_by_role("menuitem", name=self.name)
