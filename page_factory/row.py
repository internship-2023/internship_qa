from page_factory.component import Component


class Row(Component):
    @property
    def type_of(self) -> str:
        return 'row'

    def get_locator_by_role(self):
        return self.page.get_by_role("row", name=self.name)
