import allure

from page_factory.component import Component


class Button(Component):
    @property
    def type_of(self) -> str:
        return 'button'

    # def hover(self, **kwargs) -> None:
    #     with allure.step(f'Hovering over {self.type_of} with name "{self.name}"'):
    #         locator = self.get_locator(**kwargs)
    #         locator.hover()

    # def double_click(self, **kwargs):
    #     with allure.step(f'Double clicking {self.type_of} with name "{self.name}"'):
    #         locator = self.get_locator(**kwargs)
    #         locator.dblclick()

    def get_button_locator(self):
        return self.page.get_by_role("button", name=self.name)

    def get_button_by_name(self, button_name):
        return self.page.get_by_role("button", name=button_name)