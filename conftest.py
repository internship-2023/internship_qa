import pytest
from dotenv import load_dotenv
from playwright.sync_api import Page, sync_playwright
from fnmatch import fnmatch

from pages.base86_page_for_customer_staging import Base86PageForCustomerStaging
from pages.base86_start_page import Base86StartPage

import json
import os 


@pytest.fixture(scope='function')
def chromium_page() -> Page:
    with sync_playwright() as playwright:
        chromium = playwright.chromium.launch()
        yield chromium.new_page()



@pytest.fixture(scope='function')
def base86_start_page(chromium_page: Page) -> Base86StartPage:
    return Base86StartPage(chromium_page)


@pytest.fixture(scope='function')
def base86_page_for_customer_staging(chromium_page: Page) -> Base86PageForCustomerStaging:
    return Base86PageForCustomerStaging(chromium_page)


@pytest.fixture(scope='function')
def credentials_for_customer_staging():
    login = ""
    password = ""
    dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

    if os.path.exists(dotenv_path):
        load_dotenv(dotenv_path)
        login = os.getenv('login')
        password = os.getenv('password')

    if login and login.isspace():
        load_dotenv()
        login = os.getenv('login')
        password = os.getenv('password')

    return login, password


# @pytest.fixture(scope='function')
# def remove_important_parameters(credentials_for_customer_staging) -> None:
#     login, password = credentials_for_customer_staging
#     current_dir = os.path.dirname(__file__) + '\\reports\\allure-results\\'
#     for my_file in os.listdir(current_dir):
#         if fnmatch(my_file, '*-result.json'):
#             try:
#                 with open(my_file) as f:
#                     data = json.load(f)
#                     try:
#                         part_of_json = data['steps']
#
#                         for _iter_ in range(len(part_of_json)):
#                             if part_of_json[_iter_]['name'].find(login) > 0:
#                                 part_of_json[_iter_]['name'] = part_of_json[_iter_]['name'].replace(login,
#                                                                                                     "<some login>")
#                             elif part_of_json[_iter_]['name'].find(password) > 0:
#                                 part_of_json[_iter_]['name'] = part_of_json[_iter_]['name'].replace(password,
#                                                                                                     "<some password>")
#                     except:
#                         pass
#
#                     try:
#                         part_of_json = data['statusDetails']
#                         if part_of_json['trace'].find(login) > 0:
#                             part_of_json['trace'] = part_of_json['trace'].replace(login, "<some login>")
#                         elif part_of_json['trace'].find(password) > 0:
#                             part_of_json['trace'] = part_of_json['trace'].replace(password, "<some password>")
#                     except:
#                         pass
#
#                 with open(my_file, 'w') as outfile:
#                     json.dump(data, outfile, indent=4)
#             except:
#                 pass
