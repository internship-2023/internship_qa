# BASE_URL_PROD = 'https://app.base86.com'
BASE_URL_STAGE = 'https://staging.base86.com'
URL_SIGN_IN = "https://staging.base86.com/auth/sign-in"
URL_STAGING_Customer_PO = "https://staging.base86.com/customer/purchase-orders"
URL_STAGING_CustomerProducts = "https://staging.base86.com/customer/products/"
URL_STAGING_Suppliers = "https://staging.base86.com/customer/suppliers/"
SUPPLIER = "American Dental Accessories, Inc."
DRAFT_STATUS = 'Draft'
MAX_QTY = '999'



